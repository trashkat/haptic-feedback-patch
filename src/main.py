# Main Python script for the project

# entry point of the application


braille_mapping = {
    'A': ['1', '0', '0', '0', '0', '0'],
    'B': ['1', '1', '0', '0', '0', '0'],
    'C': ['1', '0', '0', '1', '0', '0'],
    'D': ['1', '0', '0', '1', '1', '0'],
    'E': ['1', '0', '0', '0', '1', '0'],
    'F': ['1', '1', '0', '1', '0', '0'],
    'G': ['1', '1', '0', '1', '1', '0'],
    'H': ['1', '1', '0', '0', '1', '0'],
    'I': ['0', '1', '0', '1', '0', '0'],
    'J': ['0', '1', '0', '1', '1', '0'],
    'K': ['1', '0', '1', '0', '0', '0'],
    'L': ['1', '1', '1', '0', '0', '0'],
    'M': ['1', '0', '1', '1', '0', '0'],
    'N': ['1', '0', '1', '1', '1', '0'],
    'O': ['1', '0', '1', '0', '1', '0'],
    'P': ['1', '1', '1', '1', '0', '0'],
    'Q': ['1', '1', '1', '1', '1', '0'],
    'R': ['1', '1', '1', '0', '1', '0'],
    'S': ['0', '1', '1', '1', '0', '0'],
    'T': ['0', '1', '1', '1', '1', '0'],
    'U': ['1', '0', '1', '0', '0', '1'],
    'V': ['1', '1', '1', '0', '0', '1'],
    'W': ['0', '1', '0', '1', '1', '1'],
    'X': ['1', '0', '1', '1', '0', '1'],
    'Y': ['1', '0', '1', '1', '1', '1'],
    'Z': ['1', '0', '1', '0', '1', '1'],
    ' ': ['0', '0', '0', '0', '0', '0']  # Space
}


def text_to_braille(input_text, braille_mapping):
# def text_to_braille(input_text):
    # need to initialize the string variable
    output_braille = []

    for char in input_text:
        braille_value = braille_mapping[char]
        # print(braille_value)
        output_braille.append(braille_value)
    # for char in the input_text
        # need to find the char value corresponding to the key
        # need to append the value of the char key to the string variable

    return output_braille


input_text = "A12"

output = text_to_braille(input_text, braille_mapping)
print(output)
